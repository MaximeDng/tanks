using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float health = 100f;
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        //transform.rotation = Quaternion.Euler(0,0,180f);
    }

    private void Update() {
        if (target == null){
            Debug.Log("No target set for " + gameObject);
            return;
        }

        Vector2 direction = target.transform.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        Quaternion targetAngle = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetAngle, 100 * Time.deltaTime);
    }

    public void TakeDamage(float amount){
        health -= amount;
        if (health <= 0f){
            Destroy(gameObject);
        }
    }
}
