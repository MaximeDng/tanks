using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TankController : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject canonGo;

    public TankShell shellGo;


    [Header("Movement")]
    public Vector3 rotationOffset = new Vector3(0,0,-180f);
    public float speed = 100f;
    public float rotationSpeed = 5f;

    public float canonRotationSpeed = 100f;


    Camera cam;
    Rigidbody2D rb;
    Transform canonTransform;

    GameObject projectileContainer;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //transform.rotation = Quaternion.Euler(rotationOffset);

        cam = Camera.main;
        canonTransform = canonGo.transform;

        projectileContainer = new GameObject("Player_Projectiles");
    }

    // Update is called once per frame
    void Update()
    {
        // Rotation
        float inputRotation = -Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
        transform.Rotate(Vector3.forward, inputRotation);
        HandleCanon();
        HandleShot();
    }

    void HandleShot(){
        if (Input.GetMouseButtonDown(0)){
            TankShell shell = Instantiate<TankShell>(shellGo, canonTransform.position, canonTransform.rotation);
            shell.transform.parent = projectileContainer.transform;
        }
    }

    void HandleCanon(){
       Vector3 mousePos = Input.mousePosition;
       Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
       targetPos.z = 0;
       Vector3 dir = targetPos - canonTransform.position;

       float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg - 90f;
       //float angle = Vector2.SignedAngle(transform.position, dir);
       Quaternion targetRot =  Quaternion.AngleAxis(angle, Vector3.forward);
       canonTransform.rotation = Quaternion.Slerp(canonTransform.rotation, targetRot, canonRotationSpeed * Time.deltaTime);
    }

    void FixedUpdate() {
        // Move 
        float yMove = Input.GetAxis("Vertical") * speed * Time.fixedDeltaTime;
        rb.velocity = transform.up * yMove;
    }
}
